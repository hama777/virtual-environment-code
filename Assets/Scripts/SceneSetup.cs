﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneSetup : MonoBehaviour
{
    /*
     * instances
     */
    public GameObject rolloutTorch;
    public GameObject cameraTorch;
    public GameObject directionalLight;

    public Boat boatScript;
    public FadePanel fadePanelScript;

    public AudioSource ambienceSoundtrack1;
    public AudioSource ambienceSoundtrack2;
    public AudioSource ambienceSoundtrack3;

    public AudioSource pipeWater1;
    public AudioSource pipeWater2;
    public AudioSource pipeWater3;
    public AudioSource pipeWater4;
    public AudioSource pipeWater5;

    public Slider dominantHandToggle;
    private const int leftHandValue = 0;
    private const int rightHandValue = 1;

    public GameObject rightVRTorch;
    public GameObject leftVRTorch;
    public GameObject noVRTorch;
    public GameObject VRAvatar;

    public TorchTurnOn torchTurnOnScript;
    
    /*
     * methods
     */
    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void SetThingsUpAndStart()
    {
        //Declare new participant
        TimestampHandler.DeclareNewParticipant();

        //Hide game objects that need to be hidden
        rolloutTorch.SetActive(false);
        //cameraTorch.SetActive(false);
        directionalLight.SetActive(false);

        //Get the boat moving
        boatScript.SetIsMoving(true);

        //Play sounds related to boat movements
        boatScript.PlayBoatCreakSoundtrack();
        boatScript.PlayBoatThroughWaterSoundtrack();

        //Play ambient sounds
        ambienceSoundtrack1.Play();
        ambienceSoundtrack2.Play();
        ambienceSoundtrack3.Play();

        //Play pipe sounds
        pipeWater1.Play();
        pipeWater2.Play();
        pipeWater3.Play();
        pipeWater4.Play();
        pipeWater5.Play();


        //Fade In 
        fadePanelScript.FadeIn();

        // activate the correct torch
        if (VRAvatar.activeInHierarchy == true)
        {
            if (dominantHandToggle.value == leftHandValue)
            {
                //leftVRTorch.SetActive(true);
                boatScript.SetActiveTorch(leftVRTorch);
            }
            else if (dominantHandToggle.value == rightHandValue)
            {
                //rightVRTorch.SetActive(true);
                boatScript.SetActiveTorch(rightVRTorch);
            }
        }
        else
        {
            //noVRTorch.SetActive(true);
            boatScript.SetActiveTorch(noVRTorch);
        }

    }

}
