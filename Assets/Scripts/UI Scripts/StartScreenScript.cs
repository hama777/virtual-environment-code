﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartScreenScript : MonoBehaviour
{
    /*
     * instances
     */
    public SceneSetup sceneSetupScript;
    public GameObject configurationPanel;
    
    private bool isStartButtonPressed = false;

    /*
     * methods
     */
    void Start()
    {
        
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.S) && !isStartButtonPressed)
        {
			GetThePartyStarted ();
        }
    }

	public void GetThePartyStarted()
	{
		//Boolean value to make sure this only executes once
		isStartButtonPressed = true;

		//Tell SceneSetup to start the experience
		sceneSetupScript.SetThingsUpAndStart();

		//Hide Start Screen and show Configuration Screen
		gameObject.SetActive(false);
		configurationPanel.SetActive(true);

		//Register start event timestamp
		TimestampHandler.Add("Experience Starts");
	}

}
