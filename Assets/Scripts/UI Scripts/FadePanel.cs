﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FadePanel : MonoBehaviour
{
    /*
     * instances
     */
    public float fadeInDuration;
    public float fadeOutDuration;
    private Image panelImage;
    
    /*
     * methods
     */
    void Start()
    {
        panelImage = GetComponent<Image>();
    }

    void Update()
    {
        //Restart experience
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        //Quit experience
        if (Input.GetKeyDown(KeyCode.Q))
        {
            Application.Quit();
            EditorApplication.isPlaying = false;
        }
    }

    public void FadeIn()
    {
        StartCoroutine(FadeInCoroutine()); 
    }

    private IEnumerator FadeInCoroutine()
    {
        float alpha = 1f;
        float startTime = Time.time;

        while (alpha > 0f)
        {
            alpha = 1 - (Time.time - startTime) / fadeInDuration;
            panelImage.color = new Color(0, 0, 0, alpha);
            yield return null;
        }
    }

    public void FadeOut()
    {
        StartCoroutine(FadeOutCoroutine());
    }

    private IEnumerator FadeOutCoroutine()
    {
        float alpha = 0f;
        float startTime = Time.time;

        while(alpha < 1f)
        {
            alpha = (Time.time - startTime) / fadeOutDuration;
            panelImage.color = new Color(0, 0, 0, alpha);
            yield return null;
        }

        //Restart the experience nce the fade is done
        //SceneManager.LoadScene(SceneManager.GetActiveScene().name);

        // Quit when experience is done
        Application.Quit();
        EditorApplication.isPlaying = false;
    }
}
