﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OnTriggerSoundEffectsPlayer : MonoBehaviour
{
    /*
     * instances
     */
    public AudioSource[] audioSourcesList;
    public float[] delayTimesList;

    public Toggle soundFXToggle;
    private bool triggerIsActive = true;

    /*
     * methods
     */
    void Start()
    {
        triggerIsActive = soundFXToggle.isOn;
        AddToggleListener();
    }

    void Update()
    {
        
    }

    //Loop over audio sources and play each one
    private void PlayAudioSources()
    {
        for(int i = 0; i < audioSourcesList.Length; ++i)
        {
            StartCoroutine(PlayAudioSourceCoroutine(i));
        }
    }

    //Coroutine for playing individual sounds
    private IEnumerator PlayAudioSourceCoroutine(int index)
    {
        yield return new WaitForSeconds(delayTimesList[index]);
        audioSourcesList[index].Play();
    }

    //Listens for when the boat hits ours trigger
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Boat" && triggerIsActive)
        {
            //Play da damn sound
            PlayAudioSources();
        }
    }

    private void AddToggleListener()
    {
        soundFXToggle.onValueChanged.AddListener(delegate
        {
            ToggleValueChanged(soundFXToggle);
        });
    }

    private void ToggleValueChanged(Toggle toggle)
    {
        triggerIsActive = toggle.isOn;
    }
}
