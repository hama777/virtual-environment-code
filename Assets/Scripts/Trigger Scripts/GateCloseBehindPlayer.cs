﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GateCloseBehindPlayer : MonoBehaviour
{
    /*
     * instances
     */
    public Transform gateTransform;
    public float gateCloseSpeed;
    public float gateCloseAngle;

    public AudioSource gateCloseSound;

    public Toggle gateCloseTriggerToggle;
    private bool triggerIsActive;

    /*
     * methods
     */
    void Start()
    {
        triggerIsActive = gateCloseTriggerToggle.isOn;
        AddToggleListener();
    }

    void Update()
    {

    }

    private IEnumerator CloseGate()
    {
        Vector3 closeOrientation = new Vector3(gateTransform.rotation.x, gateCloseAngle, gateTransform.rotation.z);
        while (Quaternion.Angle(gateTransform.rotation, Quaternion.Euler(closeOrientation)) > 0)
        {
            gateTransform.rotation = Quaternion.RotateTowards(gateTransform.rotation, Quaternion.Euler(closeOrientation), gateCloseSpeed * Time.deltaTime);
            yield return null;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Boat" && triggerIsActive)
        {
            gateCloseSound.Play();
            StartCoroutine(CloseGate());
        }
    }

    private void AddToggleListener()
    {
        gateCloseTriggerToggle.onValueChanged.AddListener(delegate
        {
            ToggleValueChanged(gateCloseTriggerToggle);
        });
    }

    private void ToggleValueChanged(Toggle toggle)
    {
        triggerIsActive = toggle.isOn;
    }
}
