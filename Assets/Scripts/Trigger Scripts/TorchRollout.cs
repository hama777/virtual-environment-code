﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TorchRollout : MonoBehaviour
{
    /*
	 * instances
	 */
    public Vector3 firstDestination;
    public float firstSpeed;
    public Vector3 secondDestination;
    public float secondSpeed;
    public float pauseTime;

    public Light torchLight;
    public GameObject cameraTorch;
    public GameObject rightHandVRTorch;
    public GameObject leftHandVRTorch;
    public GameObject fallingPipe;

    public AudioSource rollSound;
    public AudioSource clickSound;
    public AudioSource pickUpTorchSoundCamera;
    public AudioSource pickUpTorchSoundLeftHand;
    public AudioSource pickUpTorchSoundRightHand;

    public GameObject VRAvatar;
    public Slider dominantHandToggle;
    private const int leftHandValue = 0;
    private const int rightHandValue = 1;

    /*
	 * methods
     */
    void Start()
    {

    }

    void Update()
    {
        if (Input.GetKeyDown (KeyCode.P)) 
		{
            NoVRPickup();
		}
    }

    public void RolloutTorch()
    {
        
        gameObject.SetActive(true);
        StartCoroutine(RolloutCoroutine());

        //StartCoroutine(TurnOnTorchCoroutine());
    }

    /*private IEnumerator TurnOnTorchCoroutine()
    {
        yield return new WaitForSeconds(pauseTime);    // pause

        // turn on torch when it comes to a stop
        torchLight.enabled = true;

        // click soundFX
        clickSound.Play();
    }*/

    private IEnumerator RolloutCoroutine()
    {
        // roll soundFX
        rollSound.Play();

        // turn on torch
        torchLight.enabled = true;

        // click soundFX
        clickSound.Play();

        // roll up the boat
        float startTime = Time.time;
        while (!transform.position.Equals(firstDestination))
        {
            float distCovered = (Time.time - startTime) * firstSpeed;
            float journeyLength = Vector3.Distance(transform.position, firstDestination);
            float fracJourney = distCovered / journeyLength;

            transform.position = Vector3.Lerp(transform.position, firstDestination, fracJourney);
            yield return null;
        }

        rollSound.Stop();
        yield return new WaitForSeconds(pauseTime);    // pause

        // roll soundFX
        rollSound.Play();

        // roll down the boat
        while (!transform.position.Equals(secondDestination))
        {
            transform.position = Vector3.MoveTowards(transform.position, secondDestination, secondSpeed * Time.deltaTime);
            yield return null;
        }

        rollSound.Stop();
        
        yield return new WaitForSeconds(pauseTime);    // pause

        yield return new WaitForSeconds(1f);

        AttachTorch();


    }

    private void AttachTorch()
    {
        if (VRAvatar.activeInHierarchy == true)
        {
            if (dominantHandToggle.value == leftHandValue)
            {
                VRPickupLeft();
            }
            else if (dominantHandToggle.value == rightHandValue)
            {
                VRPickupRight();
            }
        }
        else
        {
        }
    }

    /*private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "RightHand")
        {
            VRPickupRight(); 
        }
        else if (other.gameObject.tag == "LeftHand")
        {
            VRPickupLeft();
        }
        else { }
    }*/

    private void VRPickupRight()
    {
        rightHandVRTorch.SetActive(true);
        pickUpTorchSoundRightHand.Play();

        fallingPipe.GetComponent<FallingPipe>().RunSink();

        gameObject.SetActive(false);
    }

    private void VRPickupLeft()
    {
        leftHandVRTorch.SetActive(true);
        pickUpTorchSoundLeftHand.Play();

        fallingPipe.GetComponent<FallingPipe>().RunSink();

        gameObject.SetActive(false);
    }

    private void NoVRPickup()
    {
        cameraTorch.SetActive(true);
        pickUpTorchSoundCamera.Play();

        fallingPipe.GetComponent<FallingPipe>().RunSink();

        gameObject.SetActive(false);
    }
}
