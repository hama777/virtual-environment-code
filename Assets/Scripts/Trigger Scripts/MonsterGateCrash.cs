﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MonsterGateCrash : MonoBehaviour
{
    /*
     * instances
     */
    public GameObject monster;
    public Transform gateTransform;
    public float gateOpenAngle;
    public float gateCrashSpeed;

    private Animator monsterAnimator;

    public AudioSource monsterCrashSound;

    public Toggle monsterCrashTriggerToggle;
    private bool triggerIsActive;

    /*
     * methods
     */
    void Start()
    {
        monsterAnimator = monster.GetComponent<Animator>();

        triggerIsActive = monsterCrashTriggerToggle.isOn;
        AddToggleListener();
    }
    
    void Update()
    {

    }

    private void MonsterJump()
    {
        monsterAnimator.Play("walk_RM");
    }

    private void CrashGateOpen()
    {
        // Register monster crashes through gate event timestamp
        TimestampHandler.Add("Monster Crashes Through Gate");

        StartCoroutine(OpenGate());
    }

    private IEnumerator OpenGate()
    {
        yield return new WaitForSeconds(1f);

        monsterCrashSound.Play();

        Vector3 openOrientation = new Vector3( gateTransform.rotation.x, gateOpenAngle, gateTransform.rotation.z);
        while (Quaternion.Angle(gateTransform.rotation, Quaternion.Euler(openOrientation)) > 0)
        {
            gateTransform.rotation = Quaternion.RotateTowards(gateTransform.rotation, Quaternion.Euler(openOrientation), gateCrashSpeed * Time.deltaTime);
            yield return null;
        }
        yield return new WaitForSeconds(1f);
        monster.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Boat" && triggerIsActive)
        {
            MonsterJump();
            CrashGateOpen();
        }
    }

    private void AddToggleListener()
    {
        monsterCrashTriggerToggle.onValueChanged.AddListener(delegate
        {
            ToggleValueChanged(monsterCrashTriggerToggle);
        });
    }

    private void ToggleValueChanged(Toggle toggle)
    {
        triggerIsActive = toggle.isOn;
    }
}
