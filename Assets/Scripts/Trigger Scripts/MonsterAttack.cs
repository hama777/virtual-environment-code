﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MonsterAttack : MonoBehaviour
{
    /*
     * instances
     */
    public GameObject monster;
    public float attackDelay;
    public GameObject pointLight;
    public float lightTimeOut;

    private Animator monsterAnimator;
    private string[] animations = {"walk_RM", "walk_RM 2", "turn90R_RM", "getHit2", "walk_RM 3",  "jump_RM", "roar", "attack1", "2HitComboAttack", "tongueAttack"};
    private int animationIndex = 0;
    private string currAnimation;
    private bool boatAppeared = false;

    public Toggle monsterAttackTriggerToggle;
    private bool triggerIsActive;


    void Start()
    {
        monsterAnimator = monster.GetComponent<Animator>();
        currAnimation = animations[animationIndex];

        triggerIsActive = monsterAttackTriggerToggle.isOn;
        AddToggleListener();
    }

    /*
     * methods
     */
    void Update()
    {
        if (boatAppeared)
        {
            HandleAnimationSequence();
        }
    }

    private void HandleAnimationSequence()
    {
        if(monsterAnimator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1 && animationIndex < animations.Length)
        {
            currAnimation = animations[animationIndex];

            if(currAnimation == "jump_RM")
            {
                // Register monster leaps at player event timestamp
                TimestampHandler.Add("Monster Leaps At Player");
            }
            monsterAnimator.Play(currAnimation);
            animationIndex++;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Boat" && triggerIsActive)
        {
            StartCoroutine(DelayedAttack());
        }
    }

    private IEnumerator DelayedAttack()
    {
        yield return new WaitForSeconds(attackDelay);
        boatAppeared = true;
        yield return new WaitForSeconds(lightTimeOut);
        pointLight.SetActive(false);
    }

    private void AddToggleListener()
    {
        monsterAttackTriggerToggle.onValueChanged.AddListener(delegate
        {
            ToggleValueChanged(monsterAttackTriggerToggle);
        });
    }

    private void ToggleValueChanged(Toggle toggle)
    {
        triggerIsActive = toggle.isOn;
    }
}
