﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FallingPipe : MonoBehaviour
{
    /*
     * instances
     */
    public Transform secondPivot;

    public float groundLevel = -0.38f;
    public float loosenAngle = -10;
    public float sinkAngle = 28;

    public float fallSpeed ;
    public float loosenSpeed;
    public float sinkSpeed;

    public AudioSource looseningSound;
    public AudioSource hitGroundSound;
    public AudioSource waterSplashSound;
    public AudioSource pipeDrowningSound;

    public Toggle pipeTriggerToggle;
    private bool triggerIsActive;

    public Boat boatScript;

    /*
     * methods
     */
    void Start()
    {
        triggerIsActive = pipeTriggerToggle.isOn;
        AddToggleListener();
    }

    void Update()
    {

    }

    public void RunFallSequence()
    {
        if (triggerIsActive)
        {
            StartCoroutine(PipeFallSequence());
        }
    }

    private IEnumerator PipeFallSequence()
    {
        //Loosening soundFX
        looseningSound.Play();

        //Loosening movement
        Vector3 finalLooseOrientation = new Vector3(loosenAngle, transform.rotation.y, transform.rotation.z);
        while (Quaternion.Angle(transform.rotation, Quaternion.Euler(finalLooseOrientation)) > 0)
        {
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(finalLooseOrientation), loosenSpeed * Time.deltaTime);
            yield return null;
        }

        yield return new WaitForSeconds(0.5f);   //Pause before pipe falls

        looseningSound.Stop();

        //Register pipe falls event timestamp
        TimestampHandler.Add("Pipe Falls");

        //Falling movement
        Vector3 destination = new Vector3(transform.position.x, groundLevel, transform.position.z);
        while (!transform.position.Equals(destination))
        {
            transform.position = Vector3.MoveTowards(transform.position, destination, fallSpeed * Time.deltaTime);
            yield return null;
        }

        //Hit ground soundFX
        hitGroundSound.Play();

        //Water splash soundFX
        waterSplashSound.Play();
    }

    public void RunSink()
    {
        //Drowning soundFX
        pipeDrowningSound.Play();

        //Run sink coroutine
        StartCoroutine(Sink());
    }

    private IEnumerator Sink()
    {
        //Rotate pipe until it's under water
        Vector3 finalSinkOrientation = new Vector3(secondPivot.rotation.x, secondPivot.rotation.y, sinkAngle);
        while (Quaternion.Angle(secondPivot.rotation, Quaternion.Euler(finalSinkOrientation)) > 0)
        {
            secondPivot.rotation = Quaternion.RotateTowards(secondPivot.rotation, Quaternion.Euler(finalSinkOrientation), sinkSpeed * Time.deltaTime);
            yield return null;
        }

        //Move boat once pipe is out of the way
        boatScript.Move();
    }

    private void OnTriggerEnter(Collider other)
    {
        //Boat hit pipe
        if(other.gameObject.tag == "Boat")
        {
            RunFallSequence();
        }
    }

    private void AddToggleListener()
    {
        pipeTriggerToggle.onValueChanged.AddListener(delegate
        {
            ToggleValueChanged(pipeTriggerToggle);
        });
    }

    private void ToggleValueChanged(Toggle toggle)
    {
        triggerIsActive = toggle.isOn;
    }

    private void RegisterEvent(string eventName)
    {

    }
}
