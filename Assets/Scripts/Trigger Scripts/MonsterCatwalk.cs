﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MonsterCatwalk : MonoBehaviour
{
    /*
     * instances
     */
    public GameObject monster;
	public GameObject pointLight;
    private Animator monsterAnimator;

    public Toggle monsterCatwalkTriggerToggle;
    private bool triggerIsActive;

    /*
     * methods
     */
    void Start()
    {
        monsterAnimator = monster.GetComponent<Animator>();

        triggerIsActive = monsterCatwalkTriggerToggle.isOn;
        AddToggleListener();

    }

    void Update()
    {
        
    }

    private IEnumerator Catwalk()
    {
		monster.SetActive(true);
		pointLight.SetActive (true);
		yield return new WaitForSeconds(3f);

        // Register monster walks infront of player event timestamp
        TimestampHandler.Add("Monster Walks Infront Of Player");

        monsterAnimator.Play("walk_RM");

        yield return new WaitForSeconds(3f);
        monster.SetActive(false);
        yield return new WaitForSeconds(10f);
        pointLight.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Boat" && triggerIsActive)
        {
            StartCoroutine(Catwalk());
        }
    }

    private void AddToggleListener()
    {
        monsterCatwalkTriggerToggle.onValueChanged.AddListener(delegate
        {
            ToggleValueChanged(monsterCatwalkTriggerToggle);
        });
    }

    private void ToggleValueChanged(Toggle toggle)
    {
        triggerIsActive = toggle.isOn;
    }
}
