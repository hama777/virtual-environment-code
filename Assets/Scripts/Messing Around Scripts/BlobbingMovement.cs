﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlobbingMovement : MonoBehaviour
{
    /*
     * instances
     */
    public float displacement = 10f;
    public float speed = 10f;
    private Vector3 initialPos;

    /*
     * methods
     */
    void Start()
    {
        initialPos = transform.position;
        StartCoroutine(Bob());
    }

    void Update()
    {
        
    }

    //Move back and forth forever
    private IEnumerator Bob()
    {
        bool forever = true;
        while (forever)
        {
            Vector3 destination = new Vector3(initialPos.x + displacement, initialPos.y, initialPos.z );
            yield return StartCoroutine(MoveToPoint(destination));     
        }
    }

    //Move to a specific point
    private IEnumerator MoveToPoint(Vector3 destination)
    {
        while (!transform.position.Equals(destination))
        {
            transform.position = Vector3.MoveTowards(transform.position, destination, speed * Time.deltaTime);
            yield return null;
        }

        if (transform.position.Equals(destination))
        {
            transform.position = initialPos;
        }
    }
}
