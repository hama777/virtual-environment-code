﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorchTurnOn : MonoBehaviour
{
    /*
     * instances
     */
    public int isRightHand;
    public GameObject torch;
    public AudioSource clickSound;

    /*
     * methods
     */
    void Start()
    {
       
    }

    void Update()
    {
        
    }

    public void TurnOn()
    {
        StartCoroutine(TurnOnTorchCoroutine());
    }

    private IEnumerator TurnOnTorchCoroutine()
    {
        yield return null;

        // turn on torch 
        torch.GetComponentInChildren<Light>().enabled = true;

        // click soundFX
        clickSound.Play();
    }

    public void SetActiveTorch(GameObject activeTorch)
    {
        torch = activeTorch;
    }
}
