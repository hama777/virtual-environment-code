﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public static class TimestampHandler 
{
    /*
     * instances
     */
    public static string timestampFilename = "timestamps.txt";
    public static List<string> events = new List<string>();
    public static List<string> times = new List<string>();

    /*
     * methods
     */
    public static void Add(string occurence)
    {
        events.Add(occurence);
        times.Add(System.DateTime.UtcNow.ToString("HH:mm, dd MMMM yyyy") + " ");
        Debug.Log(System.DateTime.UtcNow.ToString("HH:mm:ss, dd MMMM yyyy") + " - " + occurence);

        WriteToFile(occurence);
    }

    public static void DeclareNewParticipant()
    {
        string[] allLines = new string[1];
        int numParticipants;

        //Check if the file exists
        if (File.Exists(timestampFilename))
        {
            //Read number of participants
            allLines = File.ReadAllLines(timestampFilename);
            numParticipants = Int32.Parse(allLines[0]); 
        }
        else
        {
            //Create file
            StreamWriter writer = File.CreateText(timestampFilename);
            writer.Close();
        
            numParticipants = 0;
        }

        //Add a new participant
        numParticipants++;
        allLines[0] = numParticipants + "";

        //Update number of participants in file 
        File.WriteAllLines(timestampFilename, allLines);

        StreamWriter writeAppender = File.AppendText(timestampFilename);

        //Add new participant
        writeAppender.WriteLine("");
        writeAppender.WriteLine(System.DateTime.UtcNow.ToString("dd MMMM yyyy") + " -  Participant " + numParticipants);
        writeAppender.Close();
    }

    private static void WriteToFile(string line)
    {
        StreamWriter writeAppender = File.AppendText(timestampFilename);
        writeAppender.WriteLine(System.DateTime.UtcNow.ToString("HH:mm:ss") + " - " + line);
        writeAppender.Close();
    }

    public static void WriteSomeStuff()
    {
        string fileName = timestampFilename;
        StreamWriter sr;

        if (File.Exists(fileName))
        {
            Debug.Log(fileName + " already exists.");
            sr = File.AppendText(fileName);
        }
        else
        {
            sr = File.CreateText(fileName);
        }

        sr.WriteLine("This is my file.");
        sr.Close();

        Debug.Log("Wrote some stuff");
    }

    public static void ReadSomeStuff()
    {
        string fileName = timestampFilename;
        string[] rr;

        if (File.Exists(fileName))
        {
            rr = File.ReadAllLines(fileName);
            foreach(string line in rr)
            {
                Debug.Log(line);
            }
        }
        else
        {
            Debug.Log("File dont exist");
        }

    }


}
