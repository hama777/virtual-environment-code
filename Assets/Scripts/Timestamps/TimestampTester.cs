﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimestampTester : MonoBehaviour
{
    /*
     * instances
     */
    
    /*
     * methods
     */
    void Start()
    {
        
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.W))
        {
            TimestampHandler.WriteSomeStuff();
        }

        if(Input.GetKeyDown(KeyCode.R))
        {
            TimestampHandler.ReadSomeStuff();
        }
    }
}
