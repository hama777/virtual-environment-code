﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//The object that this attaches to must have a Collider that is a trigger and an Audio Source
public class SoundTriggerByProximity : MonoBehaviour
{
    /*
     * instances
     */
    private AudioSource audioSource;
    private bool triggerIsActive = true;
    
    /*
     * methods
     */
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        
    }

    //Listens for when the boat hits ours trigger
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Boat" && triggerIsActive)
        {
            //Play da damn sound
           audioSource.Play();
        }
    }
}
