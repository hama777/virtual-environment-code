﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;
using UnityEngine.UI;

public class Boat : MonoBehaviour
{
    /*
     * instances
     */
    public PathCreator pathCreator;
    public float speed = 1;
    public float distanceTravelled;

    private bool isMoving = false;
    private bool stopSoundPlayed = false;

    //public TorchTurnOn torchTurnOnScript;
    public TorchRollout torchRolloutScript;
    public FallingPipe fallingPipeScript;

    public AudioSource boatCreakSoundtrack;
    public AudioSource boatThroughWaterSoundtrack;
    public AudioSource boatHitPipeSound;

    public Slider speedSlider;
    public Text speedText;

    public float fadeDelay;
    public FadePanel fadePanelScript;

    public GameObject activeTorch;

    /*
     * methods
     */
    void Start()
    {
        speed = speedSlider.value;
        speedText.text = speed + "";
        AddSliderListener();
    }

    void Update()
    {
        if (distanceTravelled < pathCreator.path.length && isMoving)
        {
            transform.position = pathCreator.path.GetPointAtDistance(distanceTravelled);
            transform.rotation = pathCreator.path.GetRotationAtDistance(distanceTravelled);
            distanceTravelled += speed * Time.deltaTime;

            if(distanceTravelled > 200)
            {
                // turn off torch
                activeTorch.GetComponentInChildren<Light>().enabled = false;
            }
        }
        else if(distanceTravelled >= pathCreator.path.length)
        {
            if(!stopSoundPlayed)
            {
                boatHitPipeSound.Play();
                stopSoundPlayed = true;

                //Fade out to black to end the experience
                StartCoroutine(DelayFadeOut());
            }
            //Boat has reached the end of the tunnel
            boatCreakSoundtrack.Stop();
        }
        else if(!isMoving)
        {
            boatCreakSoundtrack.Stop();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.tag == "Pipe")
        {
            //Stop the boat from moving
            Stop();

            //Register boat hits pipe event timestamp
            TimestampHandler.Add("Boat Hits Pipe");

            //Play boat hit pipe soundFX
            boatHitPipeSound.Play();

            /*//Register torch switches on event timestamp
            TimestampHandler.Add("Torch Switches On");

            //Turn on torch
            torchTurnOnScript.TurnOn();*/

            //Rollout torch
            torchRolloutScript.RolloutTorch();

            /*//Register pipe sinks event timestamp
            TimestampHandler.Add("Pipe Sinks");

            //Sink the pipe
            fallingPipeScript.RunSink();*/

            //Double the boat speed
            SetBoatSpeed(speed * 2);
        }
    }

    private IEnumerator DelayFadeOut()
    {

        yield return new WaitForSeconds(fadeDelay);
        fadePanelScript.FadeOut();

        //Register experience ends event timestamp
        TimestampHandler.Add("Experience Ends");
    }

    public void Stop()
    {
        isMoving = false;
        boatCreakSoundtrack.Stop();
    }

    public void Move()
    {
        isMoving = true;
        boatCreakSoundtrack.Play();
    }

    public void SetIsMoving(bool isMovingValue)
    {
        isMoving = isMovingValue;
    }

    public void PlayBoatCreakSoundtrack()
    {
        boatCreakSoundtrack.Play();
    }

    public void PlayBoatThroughWaterSoundtrack()
    {
        boatThroughWaterSoundtrack.Play();
    }

    private void AddSliderListener()
    {
        speedSlider.onValueChanged.AddListener(delegate { SliderValueChange(); });
    }

    private void SliderValueChange()
    {
        speed = speedSlider.value;
        speedText.text = speed + "";
    }

    private void SetBoatSpeed(float newSpeedValue)
    {
        speedSlider.value = newSpeedValue;
        speed = newSpeedValue;
        speedText.text = newSpeedValue + "";

    }

    public void SetActiveTorch(GameObject torch)
    {
        activeTorch = torch;
    }
}
