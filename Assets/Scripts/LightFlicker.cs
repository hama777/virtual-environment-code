﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightFlicker : MonoBehaviour
{
    /*
     * instances
     */
    private Light wallLight;
    private bool isFlickering = true;

    public float flickerDuration;
    public float darknessProportion;
    /*
     * methods
     */
    void Start()
    {
        wallLight = GetComponent<Light>();
        StartCoroutine(FlickerCoroutine());
    }


    void Update()
    {
        
    }

    private IEnumerator FlickerCoroutine()
    {
        while(isFlickering)
        {
            //Select a random value
            float coinToss = Random.Range(0f, 1f);

            //Use the value to decide whether the light will be on or off
            if(coinToss <= darknessProportion)
            {
                wallLight.enabled = false;
            }
            else
            {
                wallLight.enabled = true;
            }

            //Select a duration for how long the light will remain on or off
            yield return new WaitForSeconds(Random.Range(0, flickerDuration));
        }
    }

    public void StartFlicker()
    {
        isFlickering = true;
        StartCoroutine(FlickerCoroutine());
    }

    public void StopFlicker()
    {
        isFlickering = false;
    }
}
